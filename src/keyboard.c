/*
* Copyright (C) 2014  Arjun Sreedharan
* License: GPL version 2 or higher http://www.gnu.org/licenses/gpl.html
*/
#include "keyboard_map.h"
#include <stdint.h>
#include <stddef.h>

/* there are 25 lines each of 80 columns; each element takes 2 bytes */
#define KEYBOARD_DATA_PORT 0x60
#define KEYBOARD_STATUS_PORT 0x64
#define IDT_SIZE 256
#define INTERRUPT_GATE 0x8e
#define KERNEL_CODE_SEGMENT_OFFSET 0x08

#define KB_BUFFER_SIZE 8192 // 8KB

#define ENTER_KEY_CODE 0x1C

unsigned int kb_echo = 1;
unsigned int kb_block = 0;
static char kb_buffer[KB_BUFFER_SIZE] = {0};
static long kb_buffer_len = 0;

extern unsigned char keyboard_map[128];
extern char port_inb(unsigned short port);
extern void port_outb(unsigned short port, unsigned char data);
extern void load_idt(void *);
extern void panic(const char *message);
extern void sprint(const char* string);
extern void sprint_char(char);
extern void sprint_back(void);

struct idt_entry_t {
    uint16_t offset_lo;
    uint16_t selector;
    uint8_t ist;
    uint8_t type_attr;
    uint16_t offset_mid;
    uint32_t offset_hi;
    uint32_t zero;
} __attribute__((packed));

struct idt_entry_t idt[IDT_SIZE];

static int register_interrupt_handler(size_t vec, void *handler, uint8_t ist, uint8_t type) {
    uint64_t p = (uint64_t)handler;

    idt[vec].offset_lo = (uint16_t)p;
    idt[vec].selector = 0x08;
    idt[vec].ist = ist;
    idt[vec].type_attr = type;
    idt[vec].offset_mid = (uint16_t)(p >> 16);
    idt[vec].offset_hi = (uint32_t)(p >> 32);
    idt[vec].zero = 0;

    return 0;
}


__attribute__((interrupt)) void keyboard_handler(void *p) {
    (void)p;

    if (kb_block)
        return;

    unsigned char status;
    char keycode;

    /* write EOI */
    port_outb(0x20, 0x20);

    status = port_inb(KEYBOARD_STATUS_PORT);
    /* Lowest bit of status will be set if buffer is not empty */
    if (status & 0x01) {
            keycode = port_inb(KEYBOARD_DATA_PORT);

            if (keycode < 0)
                return;

            char thischar;
            if (keycode == ENTER_KEY_CODE) {
                thischar = '\n';
            } else {
                thischar = keyboard_map[(char) keycode];
            }

            if (kb_echo)
                sprint_char(keyboard_map[(char) keycode]);

            if (kb_buffer_len != KB_BUFFER_SIZE) {
                kb_buffer[kb_buffer_len++] = thischar;
            } else {
                panic("Keyboard buffer overflow - TODO: find a better solution");
            }
    }
}

void idt_init(void) {
        size_t keyboard_address;
        unsigned long idt_address;
        struct {
          uint16_t limit;
          uint64_t addr;
        } __attribute__((packed)) idt_ptr;

        /* populate IDT entry of keyboard's interrupt */
        register_interrupt_handler(0x21, keyboard_handler, 0, INTERRUPT_GATE);

        /*     Ports
        *	 PIC1	PIC2
        *Command 0x20	0xA0
        *Data	 0x21	0xA1
        */

        /* ICW1 - begin initialization */
        port_outb(0x20 , 0x11);
        port_outb(0xA0 , 0x11);

        /* ICW2 - remap offset address of IDT */
        /*
        * In x86 protected mode, we have to remap the PICs beyond 0x20 because
        * Intel have designated the first 32 interrupts as "reserved" for cpu exceptions
        */
        port_outb(0x21 , 0x20);
        port_outb(0xA1 , 0x28);

        /* ICW3 - setup cascading */
        port_outb(0x21 , 0x00);
        port_outb(0xA1 , 0x00);

        /* ICW4 - environment info */
        port_outb(0x21 , 0x01);
        port_outb(0xA1 , 0x01);
        /* Initialization finished */

        /* mask interrupts */
        port_outb(0x21 , 0xff);
        port_outb(0xA1 , 0xff);

        /* fill the IDT descriptor */
        idt_address = (unsigned long)idt ;
        idt_ptr.limit = IDT_SIZE * sizeof(struct idt_entry_t) - 1;
        idt_ptr.addr = (uint64_t)idt;

        load_idt(&idt_ptr);
}

void kb_init(void)
{
        /* 0xFD is 11111101 - enables only IRQ1 (keyboard)*/
        port_outb(0x21 , 0xFD);
}

char kb_getchar(void) {
    // Iterate backwards through buffer
    for (long it = KB_BUFFER_SIZE - 1; it != -1; it--) {
        if (kb_buffer[it] != 0) {
            char res = kb_buffer[it];
            kb_buffer[it] = 0;
            kb_buffer_len--;
            return res;
        }
    }
    return 0;
}


char getch(void) {
    char res = 0;
    // Backup echo state
    unsigned int kb_echo_bak = kb_echo;
    kb_echo = 0;
    // Loop until buffer contains character
    while (res == 0) {
        res = kb_getchar();
    }
    return res;
    // Restore echo state
    kb_echo = kb_echo_bak;
    // Return resulting character
    return res;
}

unsigned int wgetuntil(char *buffer, char delimiter, char blockchar, unsigned int maxsize) {
    unsigned int currsize = 0;
    // Print buffer
    for (unsigned int it = 0; it != maxsize; it++) {
        if (buffer[it] == 0) {
            break;
        } else if (buffer[it] != '\b') {
            sprint_char(buffer[it]);
            currsize++;
        }
    }
    // Main input part
    char res = 0;
    unsigned int kb_echo_bak = kb_echo;
    kb_echo = 0;
    while (1) {
        res = getch();
        if (res == delimiter) {
            if (kb_echo_bak)
                sprint_char(delimiter);
            break;
        } else if (res == blockchar) {
            continue;
        } else if (res == '\b') {
            if (currsize != 0) {
                if (kb_echo_bak)
                    sprint_back();
                currsize--;
                buffer[currsize] = 0;
            }
        } else if (currsize == maxsize) {
            res = delimiter + 1;
            continue;
        } else {
            buffer[currsize++] = res;
            if (kb_echo_bak)
                sprint_char(res);
        }
    }
    kb_echo = kb_echo_bak;
    // Fixup last character if required
    if (currsize == maxsize) {
        buffer[currsize--] = 0;
    }
    // Return result
    return currsize;
}

char getchar(void) {
    char cbuffer[1] = {0};
    while (cbuffer[0] == 0) {
        wgetuntil(cbuffer, '\n', '\n', 1);
        sprint_char('\n');
    }
    return cbuffer[0];
}
