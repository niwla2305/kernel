// Hidden thanks to qookie: https://github.com/qookei/quack/blob/master/kernel/lib/vsnprintf.h
#ifndef VSPRINTF_H
#define VSPRINTF_H

#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>

extern void printf(const char *, ...);

#endif
