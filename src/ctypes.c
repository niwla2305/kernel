static int nums[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '~'};

int isdigit(int arg) {
    for (unsigned int it = 0; nums[it] != '~'; it++) {
        if (nums[it] == arg)
            return 1;
    }
    return 0;
}
