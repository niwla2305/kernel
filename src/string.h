#include <stddef.h>
#include <stdint.h>

extern size_t strlen(const char *s);
extern void* memset(void *dst, int ch, size_t count);
extern void* memcpy(void *dst, const void *src, size_t len);
extern char *strcpy(char *dest, const char *src);
extern char *strncpy(char *dest, const char *src, size_t num);
extern char *strcat(char *dest, const char *src);
