#! /usr/bin/env python3
from subprocess import call
from sys import argv
import sys, os

usage = f'{argv[0]} [--objs] <"c" or "obj"> <input> <output>'

#CARGS = ["-m64", "-g", "-c", "-Wimplicit-function-declaration", "-fno-stack-protector", "-ffreestanding"]
#ASMARGS = ["-F", "dwarf", "-g", "-f", "elf64"]
CARGS = ["-m64", "-g", "-c", "-fno-stack-protector", "-fno-pic", "-mno-sse", "-mno-sse2", "-mno-mmx", "-mno-80387", "-mno-red-zone", "-mcmodel=kernel", "-ffreestanding", "-fno-stack-protector", "-fno-omit-frame-pointer"]
ASMARGS = ["-F", "dwarf", "-g", "-f", "elf64"]


if len(argv) > 1 and argv[1] == "--objs":
    objects = []
    print("Compiling objects...")
    for filename in os.listdir("src"):
        res = 0
        filepath = f"src/{filename}"
        fileobj = f"objects/{os.path.splitext(filename)[0]}.o"
        print(f"Compiling {filename}...")
        if filename[-4:] == ".asm":
            objects.append(fileobj)
            res = call(["nasm"] + ASMARGS + ["-o", fileobj] + [filepath])
        elif filename[-2:] == ".c":
            objects.append(fileobj)
            res = call(["clang"] + CARGS + ["-o", fileobj] + [filepath])
        if res != 0:
            sys.exit(res)
    print("Done compiling objects!")
    print()
    del argv[1]

if len(argv) < 3:
    print("To link the kernel with an program:")
    print(usage)
    sys.exit(0)

if not "objects" in locals():
    objects = []
    for filename in os.listdir("objects"):
        if filename[-2:] == ".o":
            objects.append(f"objects/{filename}")

if objects == []:
    print("Please compile objects first:")
    print(f'{argv[0]} --objs "' + '" "'.join(argv[1:]) + '"')
    sys.exit(1)

if argv[1] == "c":
    print("Compiling user program...")
    objects.append(f"{os.path.splitext(argv[2])[0]}.o")
    res = call(["gcc"] + CARGS + ["-I", "src", "-o", objects[-1], argv[2]])
    if res != 0:
        sys.exit(res)
    print()
elif argv[1] == "obj":
    objects.append(argv[2])
else:
    print('Invalid usage: Input type must be "c" or "obj":')
    print(usage)
    sys.exit(1)

print("Linking...")
res = call(["ld", "-m", "elf_x86_64", "-T", "link.ld", "-o", argv[3]] + objects)
if res != 0:
    sys.exit(res)
print()

print(f'Kernel generated at "{argv[3]}"!')
