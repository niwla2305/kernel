#! /bin/sh

./compile.py --objs c examples/program.c build/kernel.elf &&
./mkbootable.sh build/kernel.elf build/bootable.img &&
qemu-system-x86_64 -hda build/bootable.img
