#include <simplecore.h>
#include <keyboard.h>
#include <stdio.h>
#include <string.h>
#include <printf.h>
#include <helpers.h>


int inputdemo(void) {
    // Initialise 64B sized buffer
    char buffer[20];
    // Ask questions
    printf("Type in your full name: ");
    memset(buffer, 0, sizeof(buffer));
    unsigned int collectedchars = wgetuntil(buffer, ' ', '\n', 16);
    printf("\nWait! Stop! I decided the first name is enough for me. Or do you REALLY want me to know your full name?\n");
    if (yesno("Yes? No? ")) {
        buffer[collectedchars] = ' '; // Append space
        printf("Well then...\n");
        printf("Full name: ");
        wgetuntil(buffer, '\n', 0, 20);
    }
    // Print summarisation
    printf("Ok, your name is %s!\n", buffer);
    // Exit
    return 0;
}

int ramdump(void) {
    if (!yesno("This could crash your computer. Press any key to interrupt.\nContinue (y/n)? ")) {
        return 1;
    }
    scursor_hide();
    sclear();
    char *ram = (char*)0x00000;
    char adressstr[16];
     for (long it = 0; ; it++) {
        // Print next character
        sprint_char(ram[it]);
        // Display current position
        scursor_save(0);
        scursor_center(0, sizeof(adressstr));
        itoa(it, adressstr, 10);
        sprint("| ");
        sprint(adressstr);
        sprint(" |");
        scursor_restore(0);
        // Check for new input in keyboard buffer
        if (kb_getchar() != 0) {
            break;
        }
    }
    scursor_show();
    return 0;
}



void overview(void) {
    printf("Try one of these demos:\n");
    printf(" 0) Show this overview\n");
    printf(" 1) User input demo\n");
    printf(" 2) Dump RAM to screen\n");
}

int program(void) {
    char uin[1] = {};
    printf("Welcome!\n");
    // Show overview once
    overview();
    // Start loop
    while (1) {
        memset(uin, 0, sizeof(uin));
        // Get user input
        printf("\n? ");
        wgetuntil(uin, '\n', 0, sizeof(uin));
        // Run program if possible
        if (uin[0] == '0') {
            overview();
        } else if (uin[0] == '1') {
            inputdemo();
        } else if (uin[0] == '2') {
            ramdump();
        } else {
            printf("Invalid input!");
        }
    }
}
